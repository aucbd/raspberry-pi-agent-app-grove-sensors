from json import dumps as json_dumps, loads as json_loads
from typing import Any, Dict, Union

from grove.factory import Factory
from grove.gpio import GPIO
from grove.grove_temperature_humidity_sensor import DHT
from grove.grove_ultrasonic_ranger import GroveUltrasonicRanger

log: 'Log'
settings: 'Settings'
Reading = Union[str, int, float, bool]


def sensor_read(sensor_arg: Dict[str, Union[int, str]]) -> Reading:
    sensor: Any
    reading: Any

    if sensor_arg["type"] == "ultrasonic_ranger":
        sensor = GroveUltrasonicRanger(abs(sensor_arg["port"]))
        return sensor.get_distance()
    elif sensor_arg["type"] in ("dht11", "temperature_humidity_sensor"):
        sensor = DHT("11", abs(sensor_arg["port"]))
        reading = sensor.read()
        return f"{reading[0]} {reading[1]}"
    elif sensor_arg["type"] == "pir_motion":
        sensor = Factory.getGpioWrapper("PIRMotion", abs(sensor_arg["port"]))
        return sensor.has_motion()
    elif sensor_arg["type"] == "gpio":
        sensor = GPIO(abs(sensor_arg["port"]), GPIO.IN)
        return sensor.read()
    else:
        return f"{sensor_arg['type']} NOT IMPLEMENTED"


def str2port(port_str: str) -> int:
    port_int = int(port_str[1:])

    if port_str[0].upper() == 'D':
        port_int *= 1
    elif port_str[0].upper() == 'A':
        port_int *= -1

    return port_int


def callback_sensors(ag: 'Agent', _user_data, msg: 'Message'):
    try:
        msg_json: Dict[str, str] = json_loads(msg.payload.decode())
        command: str = msg_json["command"]

        if command == "set":
            settings["grove_sensors"]["sensors"][msg_json["name"]] = {
                "type": msg_json["type"].lower(),
                "port": str2port(msg_json["port"])
            }
        elif command == "remove":
            del settings["grove_sensors"]["sensors"][msg_json["name"]]
        elif command == "read":
            ag.publish(msg_json["name"], sensor_read(settings["grove_sensors"]["sensors"][msg_json["name"]]))
        elif command == "list":
            ag.publish("console", json_dumps(settings["grove_sensors"]["sensors"]))

    except (BaseException, Exception) as err:
        log.write(f"GROVE SENSORS ERROR:{repr(err)}")
        ag.publish("console", f"GROVE SENSORS ERROR:{repr(err)}")


def app(settings_arg: 'Settings', log_arg: 'Log', agent: 'Agent'):
    agent.subscribe(settings["grove_sensors"]["sensors_topic"])
    agent.message_callback_add(settings["grove_sensors"]["sensors_topic"], callback_sensors)

    while True:
        agent.loop(1)
